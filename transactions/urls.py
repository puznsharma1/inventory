from django.urls import path

from . import views
urlpatterns= [path('',views.index, name='index'),
              path('login/', views.login, name='login'),
              path('register/', views.register, name='register'),
              path('purchase/', views.purchase, name='purchase'),
              path('items/', views.items, name='items'),
              path('add_items/', views.add_item, name='add_items'),
              path('vendordetails/', views.vendordetails, name='vendordetails'),
              path('add_vendors/', views.add_vendors, name='add_vendors'),
              path('customer/', views.customer, name='customer'),
              path('add_customers/', views.add_customers, name='add_customers'),
              path('add_purchase/', views.add_purchase, name='add_purchase'),
              path('loss&damage/', views.loss_damage, name='loss&damage'),
              path('sales/', views.sales, name='sales'),
              path('salesreturn/', views.sales_return, name='salesreturn'),
              path('purchasereturn/', views.purchase_return, name='purchasereturn'),
              path('rack/', views.rack, name='rack'),
              path('stocktransfer/', views.stocktransfer, name='stocktransfer'),
              path('dashboard/', views.dashboard, name='dashboard'),
              path('export_pdf/', views.export_pdf, name='export_pdf'),
              ]