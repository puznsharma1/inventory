from django.db import models


class Items(models.Model):
    username = models.CharField(max_length=200)
    local_name = models.CharField(max_length=50)
    item_name = models.CharField(max_length=200)
    item_code = models.CharField(max_length=200)
    item_limit = models.IntegerField()
    item_amount = models.IntegerField()
    pan_number = models.CharField(max_length=100)
    quantity = models.IntegerField(null=True)

    def __str__(self):
        return self.item_name


class vendor_details(models.Model):
    username = models.CharField(max_length=200)
    vendor_name = models.CharField(max_length=50)
    company_name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    company_contact = models.IntegerField()
    vendor_contact = models.IntegerField()
    pan_number = models.CharField(max_length=100)

    def __str__(self):
        return self.vendor_name


class customer_details(models.Model):
    username = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=50)
    company_name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    company_contact = models.IntegerField()
    customer_contact = models.IntegerField()
    pan_number = models.CharField(max_length=100)

    def __str__(self):
        return self.customer_name


class Purchase(models.Model):
    item = models.ForeignKey(Items, on_delete=models.CASCADE, null=True)
    vendor = models.ForeignKey(vendor_details, on_delete=models.CASCADE, null=True)
    batch_number = models.CharField(max_length=200)
    value = models.IntegerField()
    units = models.IntegerField()
    quantity = models.IntegerField()
    date = models.DateField(auto_now_add=True)
    rack = models.CharField(max_length=200)
    bill_image = models.CharField(max_length=200)
    bill_number = models.CharField(max_length=200)
    total = models.IntegerField()


    def __str__(self):
        return self.item.username


class Loss(models.Model):
    item = models.ForeignKey(Items, on_delete=models.CASCADE, )
    purchase = models.ForeignKey(Purchase, on_delete=models.CASCADE, )
    price = models.IntegerField()
    quantity = models.IntegerField()

    def __str__(self):
        return self.item.username


class Sales_Info(models.Model):
    item = models.ForeignKey(Items, on_delete=models.CASCADE, )
    customer = models.ForeignKey(customer_details, on_delete=models.CASCADE, )
    user_name = models.CharField(max_length=200)
    price = models.CharField(max_length=200)
    units = models.IntegerField()
    bill_number = models.CharField(max_length=200)
    quantity = models.CharField(max_length=200, )
    total = models.IntegerField()


    def __str__(self):
        return self.item.username


class Sales_Return(models.Model):
    item = models.ForeignKey(Items, on_delete=models.CASCADE, )
    customer = models.ForeignKey(customer_details, on_delete=models.CASCADE, )
    user_name = models.CharField(max_length=200)
    batch = models.ForeignKey(Purchase, on_delete=models.CASCADE, )
    date = models.DateField(auto_now_add=True)
    bill_number = models.CharField(max_length=200)
    quantity = models.CharField(max_length=200)

    def __str__(self):
        return self.item.username


class Purchase_Return(models.Model):
    item = models.ForeignKey(Items, on_delete=models.CASCADE, )
    vendor = models.ForeignKey(vendor_details, on_delete=models.CASCADE, )
    user_name = models.CharField(max_length=200)
    batch = models.ForeignKey(Purchase, on_delete=models.CASCADE, )
    date = models.DateField(auto_now_add=True)
    bill_number = models.CharField(max_length=200)
    quantity = models.CharField(max_length=200)

    def __str__(self):
        return self.item.username


class Rack(models.Model):
    rack_no = models.CharField(max_length=200)
