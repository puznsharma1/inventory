from django.contrib import admin


# Register your models here.
from transactions.models import Items, vendor_details, customer_details,Purchase,Loss,Sales_Info,Sales_Return

admin.site.register(Items),
admin.site.register(vendor_details),
admin.site.register(customer_details),
admin.site.register(Purchase),
admin.site.register(Loss),
admin.site.register(Sales_Info),
admin.site.register(Sales_Return),


