from datetime import datetime

from django.contrib import auth, messages
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404

from transactions.models import Items, vendor_details, customer_details, Purchase, Loss, Sales_Info, Sales_Return, \
    Purchase_Return, Rack

from django.template.loader import render_to_string
import tempfile
from weasyprint import  HTML


# from transactions.models import vendor_details
#
# from transactions.models import customer_details
#
# from transactions.models import Purchase


def index(request):
    return render(request, 'blank.html')


def dashboard(request):
    return render(request, 'index.html')


def login(request):
    if request.method == "POST":
        user_name = request.POST['user_name']
        password = request.POST['password']

        user = auth.authenticate(username=user_name, password=password)

        if user is not None:
            auth.login(request, user)
            return redirect('purchase')
        else:
            messages.info(request, 'invalid username or password')
            return redirect('login')

    else:
        return render(request, 'login.html')


def register(request):
    if request.method == "POST":
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        password = request.POST['password']
        email = request.POST['email']
        username = request.POST['username']

        if username is None:
            messages.info(request, 'Email should be 1-10')
            return render(request, 'register.html')


        else:
            if User.objects.filter(username=username).exists():
                messages.info(request, 'username taken')
                return redirect('register')

            else:
                user = User.objects.create_user(first_name=first_name, last_name=last_name,
                                                password=password, email=email, username=username)
                user.save()
                messages.info(request, 'registration successful')

                return redirect('login')
    else:
        return render(request, 'register.html')


def purchase(request):
    vendors = vendor_details.objects.all()
    purchase = Purchase.objects.all()

    items = Items.objects.all()

    args = {'vendors': vendors, 'items': items, 'purchase': purchase}
    return render(request, 'purchase.html', args)


def items(request):
    list = Items.objects.all()
    return render(request, 'items.html', {'list': list})


def add_item(request):
    if request.method == "POST":
        username = request.POST['username']
        item_name = request.POST['item_name']
        item_limit = request.POST['item_limit']
        local_name = request.POST['local_name']
        item_amount = request.POST['item_amount']
        item_code = request.POST['item_code']
        pan_number = request.POST['pan_number']
        items = Items.objects.create(username=username, item_limit=item_limit, item_name=item_name,
                                     local_name=local_name,
                                     item_amount=item_amount, item_code=item_code, pan_number=pan_number)
        items.save()
        messages.info(request, 'item data added')
        list = Items.objects.all()
        return render(request, 'items.html', {'list': list})


    else:
        list = Items.objects.all()
        return render(request, 'items.html', {'list': list})


def add_vendors(request):
    if request.method == "POST":
        username = request.POST['username']
        company_name = request.POST['company_name']
        vendor_name = request.POST['vendor_name']
        email = request.POST['email']
        company_contact = request.POST['company_contact']
        vendor_contact = request.POST['vendor_contact']
        pan_number = request.POST['pan_number']

        vendors = vendor_details.objects.create(username=username, vendor_name=vendor_name, company_name=company_name,
                                                email=email, company_contact=company_contact,
                                                vendor_contact=vendor_contact, pan_number=pan_number)
        vendors.save()
        messages.info(request, 'item data added')

        vendors_list = Items.objects.all()
        return render(request, 'vendordetails.html', {'vendors_list': vendors_list})
    else:
        return redirect('vendordetails')


def vendordetails(request):
    vendors = vendor_details.objects.all()
    return render(request, 'vendordetails.html', {'vendors': vendors})


def add_customers(request):
    if request.method == "POST":
        username = request.POST['username']
        company_name = request.POST['company_name']
        customer_name = request.POST['customer_name']
        email = request.POST['email']
        company_contact = request.POST['company_contact']
        customer_contact = request.POST['customer_contact']
        pan_number = request.POST['pan_number']

        customers = customer_details.objects.create(username=username, customer_name=customer_name,
                                                    company_name=company_name,
                                                    email=email, company_contact=company_contact,
                                                    customer_contact=customer_contact, pan_number=pan_number)
        customers.save()
        messages.info(request, 'customer data added')

        customer_list = Items.objects.all()
        return render(request, 'customer.html', {'customer_list': customer_list})
    else:
        return redirect('customer')


def customer(request):
    customer_list = customer_details.objects.all()
    return render(request, 'customer.html', {'customer_list': customer_list})


def add_purchase(request):
    if request.method == "POST":
        vendor_name = request.POST['vendor_name']
        # vendor_details= request.POST['vendor_details']
        item_name = request.POST['item_name']
        units = request.POST['units']
        value = request.POST['value']
        quantity = request.POST['quantity']
        rack = request.POST['rack']
        bill_image = request.POST['bill_image']
        bill_number = request.POST['bill_number']
        batch_number = request.POST['batch_number']
        total = int(quantity) * int(value)
        print(item_name)
        print(value)
        print(total)

        purchase = Purchase.objects.create(vendor=vendor_details.objects.filter(vendor_name=vendor_name).first(),
                                           item=Items.objects.filter(item_name=item_name).first(), units=units,
                                           value=value, quantity=quantity, total=total,
                                           rack=rack, bill_image=bill_image, bill_number=bill_number,
                                           batch_number=batch_number,
                                           )
        purchase.save()
        messages.info(request, 'purchase data added')

        print(purchase.item_id)
        xyz = Items.objects.get(id=purchase.item_id)
        print(xyz.quantity)
        xyz.quantity = int(xyz.quantity) + int(purchase.quantity)
        # print(xyz.quantity)
        Items.objects.filter(id=purchase.item_id).update(quantity=xyz.quantity)
        # xyz.quantity= int(pur.quantity)

        vendors = vendor_details.objects.all()
        items = Items.objects.all()
        args = {'vendors': vendors, 'items': items}
        return render(request, 'purchase.html', args)



    else:
        return redirect('purchase')


def loss_damage(request):
    if request.method == "POST":
        item_name = request.POST['item_name']
        price = request.POST['price']
        quantity = request.POST['quantity']
        batch_number = request.POST['batch_number']

        loss = Loss.objects.create(purchase=Purchase.objects.filter(batch_number=batch_number).first(),
                                   item=Items.objects.filter(item_name=item_name).first(), price=price,
                                   quantity=quantity)
        loss.save()
        messages.info(request, 'loss data added')

        purchase = Purchase.objects.all()
        items = Items.objects.all()
        args = {'purchase': purchase, 'items': items}
        return render(request, 'loss&damage.html', args)



    else:
        purchase = Purchase.objects.all()
        items = Items.objects.all()
        loss = Loss.objects.all()
        args = {'purchase': purchase, 'items': items, 'loss': loss}
        return render(request, 'loss&damage.html', args)


def sales(request):
    if request.method == "POST":
        user_name = request.POST['user_name']
        customer_name = request.POST['customer_name']
        item_name = request.POST['item_name']
        price = request.POST['price']
        units = request.POST['units']
        quantity = request.POST['quantity']
        bill_number = request.POST['bill_number']
        total = int(quantity) * int(price)

        sales = Sales_Info.objects.create(customer=customer_details.objects.filter(customer_name=customer_name).first(),
                                          item=Items.objects.filter(item_name=item_name).first(), price=price,
                                          quantity=quantity,
                                          bill_number=bill_number, user_name=user_name, units=units, total=total, )
        sales.save()
        messages.info(request, 'sales data added')
        xyz = Items.objects.get(id=sales.item_id)
        print(xyz.quantity)
        xyz.quantity = int(xyz.quantity) - int(sales.quantity)
        # print(xyz.quantity)
        Items.objects.filter(id=sales.item_id).update(quantity=xyz.quantity)

        customer = customer_details.objects.all()
        items = Items.objects.all()
        args = {'customer': customer, 'items': items}
        return render(request, 'sales.html', args)



    else:
        customer = customer_details.objects.all()
        items = Items.objects.all()
        sales = Sales_Info.objects.all()
        args = {'customer': customer, 'items': items, 'sales': sales}
        return render(request, 'sales.html', args)


def sales_return(request):
    if request.method == "POST":
        user_name = request.POST['user_name']
        customer_name = request.POST['customer_name']
        item_name = request.POST['item_name']
        date = request.POST['date']
        batch_number = request.POST['batch_number']
        quantity = request.POST['quantity']
        bill_number = request.POST['bill_number']

        salesreturn = Sales_Return.objects.create(
            customer=customer_details.objects.filter(customer_name=customer_name).first(),
            item=Items.objects.filter(item_name=item_name).first(), date=date, quantity=quantity,
            bill_number=bill_number, user_name=user_name,
            batch=Purchase.objects.filter(batch_number=batch_number).first()
        )
        salesreturn.save()
        messages.info(request, 'salesreturn data added')
        messages.info(request, 'sales data added')
        xyz = Items.objects.get(id=salesreturn.item_id)
        print(xyz.quantity)
        xyz.quantity = int(xyz.quantity) + int(salesreturn.quantity)
        # print(xyz.quantity)
        Items.objects.filter(id=salesreturn.item_id).update(quantity=xyz.quantity)

        customer = customer_details.objects.all()
        purchase = Purchase.objects.all()
        items = Items.objects.all()
        args = {'customer': customer, 'items': items, 'purchase': purchase}
        return render(request, 'salesreturn.html', args)



    else:
        customer = customer_details.objects.all()
        items = Items.objects.all()
        purchase = Purchase.objects.all()
        salesreturn = Sales_Return.objects.all()
        args = {'customer': customer, 'items': items, 'purchase': purchase, 'salesreturn': salesreturn}
        return render(request, 'salesreturn.html', args)


def purchase_return(request):
    if request.method == "POST":
        user_name = request.POST['user_name']
        vendor_name = request.POST['vendor_name']
        item_name = request.POST['item_name']
        date = request.POST['date']
        batch_number = request.POST['batch_number']
        quantity = request.POST['quantity']
        bill_number = request.POST['bill_number']

        purchasereturn = Purchase_Return.objects.create(
            vendor=vendor_details.objects.filter(vendor_name=vendor_name).first(),
            item=Items.objects.filter(item_name=item_name).first(), date=date, quantity=quantity,
            bill_number=bill_number, user_name=user_name,
            batch=Purchase.objects.filter(batch_number=batch_number).first()
        )
        purchasereturn.save()
        messages.info(request, 'purchasereturn data added')

        purchasereturn = Purchase_Return.objects.all()
        vendors = vendor_details.objects.all()
        purchase = Purchase.objects.all()
        items = Items.objects.all()
        args = {'vendors': vendors, 'items': items, 'purchase': purchase, 'purchasereturn': purchasereturn}
        return render(request, 'purchasereturn.html', args)



    else:
        purchasereturn = Purchase_Return.objects.all()
        vendors = vendor_details.objects.all()
        purchase = Purchase.objects.all()
        items = Items.objects.all()
        args = {'vendors': vendors, 'items': items, 'purchase': purchase, 'purchasereturn': purchasereturn}
        return render(request, 'purchasereturn.html', args)


def rack(request):
    if request.method == "POST":
        rack_no = request.POST['rack_no']
        racks = Rack.objects.create(rack_no=rack_no)
        racks.save()
        return render(request, 'rack.html')

    else:
        return render(request, 'rack.html')


def stocktransfer(request):
    purchase = Purchase.objects.all()
    sales = Sales_Info.objects.all()

    args = {'purchase': purchase, 'sales': sales, }
    return render(request, 'stocktransfer.html', args)

def export_pdf(request):
    response= HttpResponse(content_type= 'application/pdf')
    response['Content-Disposition'] = 'attachment' + \
        str(datetime.now())+'.pdf'
    response['Content-Transfer-Encoding']= 'binary'

    html_string= render_to_string('pdf-output.html',{'quantity':0})
    html=HTML(string=html_string)
    result = html.write_pdf()

    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()

        output= open(output.name, 'rb')
        response.write(output.read())

    return response
